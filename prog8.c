#include <stdio.h>
#include <GL/glut.h>

#define outcode int
double xmin=50,ymin=50, xmax=100,ymax=100;
double xvmin=200,yvmin=200,xvmax=300,yvmax=300;

const int TOP = 1;
const int RIGHT = 2;
const int BOTTOM = 4;
const int LEFT = 8;

outcode ComputeOutCode (double x, double y)
{
	outcode code = 0;
	if (y > ymax)
		code |= TOP;
	else if (y < ymin)
		code |= BOTTOM;
	if (x > xmax)
		code |= RIGHT;
	else if (x < xmin)
		code |= LEFT;
	return code;
}

void CohenSutherlandLineClipAndDraw (double x0, double y0,double x1, double y1)
{

	outcode outcode0, outcode1, outcodeOut;
	int accept = 0, done = 0;
	outcode0 = ComputeOutCode (x0, y0);
	outcode1 = ComputeOutCode (x1, y1);
	do{
		if (!(outcode0 | outcode1))
		{
			accept = 1;
			done = 1;
		}
		else if (outcode0 & outcode1)
			done = 1;
		else
		{
			double x, y;
			outcodeOut = outcode0? outcode0: outcode1;
			if (outcodeOut & TOP)
			{
				x = x0 + (x1 - x0) * (ymax - y0)/(y1 - y0);
				y = ymax;
			}
			else if (outcodeOut & BOTTOM)
			{
				x = x0 + (x1 - x0) * (ymin - y0)/(y1 - y0);
				y = ymin;
			}
			else if (outcodeOut & RIGHT)
			{
				y = y0 + (y1 - y0) * (xmax - x0)/(x1 - x0);
				x = xmax;
			}
			else
			{
				y = y0 + (y1 - y0) * (xmin - x0)/(x1 - x0);
				x = xmin;
			}
			if (outcodeOut == outcode0)
			{
				x0 = x;
				y0 = y;
				outcode0 = ComputeOutCode (x0, y0);
			}
			else
			{
				x1 = x;
				y1 = y;
				outcode1 = ComputeOutCode (x1, y1);
			}
		}
	}while (!done);

	if (accept)
	{
		glColor3f(1, 0, 0);
		glBegin(GL_LINE_LOOP);
		glVertex2f(xmin + 150, ymin + 150);
		glVertex2f(xmax + 150, ymin + 150);
		glVertex2f(xmax + 150, ymax + 150);
		glVertex2f(xmin + 150, ymax + 150);
		glEnd();
		glColor3f(0,0,1);
		glBegin(GL_LINES);
		glVertex2d (x0 + 150, y0 + 150);
		glVertex2d (x1 + 150, y1 + 150);
		glEnd();
	}
}

void display()
{
	double x0=120,y0=10,x1=40,y1=130;
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1,0,0);
	glBegin(GL_LINES);
		glVertex2d (x0, y0);
		glVertex2d (x1, y1);
		glVertex2d (60,20);
		glVertex2d (80,120);
	glEnd();
	glColor3f(0, 0, 1);
	glBegin(GL_LINE_LOOP);
		glVertex2f(xmin, ymin);
		glVertex2f(xmax, ymin);
		glVertex2f(xmax, ymax);
		glVertex2f(xmin, ymax);
	glEnd();
	CohenSutherlandLineClipAndDraw(x0,y0,x1,y1);
	CohenSutherlandLineClipAndDraw(60,20,80,120);
	glFlush();
}
void myinit()
{
	glClearColor(1,1,1,1);
	glColor3f(1,0,0);
	glPointSize(1);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0,500,0,500);
}
void main(int argc, char** argv)
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
	glutInitWindowSize(500,500);
	glutInitWindowPosition(0,0);
	glutCreateWindow("Cohen Suderland Line Clipping Algorithm");
	glutDisplayFunc(display);
	myinit();
	glutMainLoop();
}
