#include <GL/glut.h>

void draw_pixel(GLint cx, GLint cy)
{
    glColor3f(1,0,0);
    glBegin(GL_POINTS);
    glVertex2i(cx,cy);
    glEnd();
}

void plotpixels(GLint h, GLint k, GLint x, GLint y)
{
	draw_pixel(x+h,y+k);
	draw_pixel(-x+h,y+k);
	draw_pixel(x+h,-y+k);
	draw_pixel(-x+h,-y+k);
	draw_pixel(y+h,x+k);
	draw_pixel(-y+h,x+k);
	draw_pixel(y+h,-x+k);
	draw_pixel(-y+h,-x+k);
}

void draw_circle(GLint h, GLint k, GLint r)
{
	 GLint d =  1-r, x=0, y=r;
	 while(y > x)
	 {
		 plotpixels(h,k,x,y);
		 if(d < 0)
            d+=2*x+2;
		 else
		 {
             d+=2*(x-y)+2;
             --y;
		 }
		 ++x;
	 }
	 plotpixels(h,k,x,y);
}

void draw_cylinder()
{
	GLint xc=100, yc=100, r=50;
	GLint i,n=50;
	for(i=0;i<n;i+=3)
	   draw_circle(xc,yc+i,r);
}

void parallelepiped(int x1,  int x2,int y1, int y2, int y3, int y4)
{
    glColor3f(0, 0, 1);
    glBegin(GL_LINE_LOOP);
    glVertex2i(x1,y1);
    glVertex2i(x2,y2);
    glVertex2i(x2,y3);
    glVertex2i(x1,y4);
    glEnd();
}

void draw_parallelepiped()
{
    int x1=200,x2=300,y1=100,y2=100,y3=175,y4=175;
    GLint i,n=40;
    for(i=0;i<n;i+=2)
        parallelepiped(x1+i,x2+i,y1+i,y2+i,y3+i,y4+i);
}

void myinit()
{
	glClearColor(1,1,1,0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0,500,0,500);
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1,0,0);
	glPointSize(2);
	draw_cylinder();
	draw_parallelepiped();
	glFlush();
}

void main(int argc, char **argv)
{
    glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowPosition(50,50);
	glutInitWindowSize(500,500);
	glutCreateWindow("Cylinder and parallelePiped Display by Extruding Circle and Quadrilaterl ");
	myinit();
	glutDisplayFunc(display);
	glutMainLoop();
}
