#include <GL/glut.h>

void wall (double thickness)
{
	glPushMatrix();
	glTranslated (0.5, 0.5 * thickness, 0.5);
	glScaled (1, thickness, 1);
	glutSolidCube (1);
	glPopMatrix();
}

void tableLeg (double thick, double len)
{
	glPushMatrix();
	glTranslated (0, len/2, 0);
	glScaled (thick, len, thick);
	glutSolidCube (1);
	glPopMatrix();
}

void table (double topWid, double topThick, double legThick, double legLen)
{
	glPushMatrix();
	glTranslated (0, legLen, 0);
	glScaled(topWid, topThick, topWid);
	glutSolidCube (1);
	glPopMatrix();

	double dist = topWid - legThick;
	glPushMatrix();
	glTranslated (dist/2, 0, dist/2);
	tableLeg (legThick, legLen);
	glTranslated (0, 0, -dist);
	tableLeg (legThick, legLen);
	glTranslated (-dist, 0, dist);
	tableLeg (legThick, legLen);
	glPopMatrix();
}

void displaySolid ()
{
    glClearColor (1, 1, 1, 0);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	gluLookAt (2, 1, 2, 0, 0.2, 0, 0, 1, 0);

	GLfloat mat_ambient[] = {0.7f, 0.7f, 0.7f, 1.0f};
	GLfloat mat_diffuse[] = {0.5f, 0.5f, 0.5f, 1.0f};
	GLfloat mat_specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat mat_shininess[] = {50.0f};
	glMaterialfv (GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv (GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv (GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv (GL_FRONT, GL_SHININESS, mat_shininess);

    GLfloat light_position[] = {2.0f, 6.0f, 3.0f, 0.0f};
	GLfloat lightIntensity[] = {0.7f, 0.7f, 0.7f, 1.0f};
    glLightfv (GL_LIGHT0, GL_POSITION, light_position);
	glLightfv (GL_LIGHT0, GL_DIFFUSE, lightIntensity);

    glPushMatrix();
    glTranslated (0.4, 0, 0.4);
    table (0.6, 0.02, 0.02, 0.3);
    glPopMatrix();

	glPushMatrix();
	glTranslated (0.6, 0.4, 0.4);
	glRotated (30, 0, 1, 0);
	glutSolidTeapot (0.08);
	glPopMatrix ();

	wall (0.02);
	glPushMatrix();
	glRotated (90, 0, 0, 1);
	wall (0.02);
	glPopMatrix();

	glPushMatrix();
	glRotated (-90, 1, 0, 0);
	wall (0.02);
	glPopMatrix();

	glFlush();
}

void myinit()
{
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	glOrtho (-1, 1, -1, 1, 1, 100);
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity();
}
void main (int argc, char ** argv)
{
	glutInit (&argc, argv);
	glutInitDisplayMode (GLUT_SINGLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowPosition (100, 100);
	glutInitWindowSize (500, 500);
	glutCreateWindow ("Simple shaded scene consisting of a tea pot on a table");
	glutDisplayFunc (displaySolid);
    glEnable (GL_DEPTH_TEST);
	glEnable (GL_LIGHTING);
	glEnable (GL_LIGHT0);
    glEnable (GL_NORMALIZE);
	glShadeModel (GL_SMOOTH);
    myinit();
	glutMainLoop();
}
