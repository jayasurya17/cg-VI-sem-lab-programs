#include <stdio.h>
#include <GL/glut.h>

double xmin=50,ymin=50, xmax=100,ymax=100;
double xvmin,yvmin,xvmax,yvmax;

int cliptest(double p, double q, double *t1, double *t2)
{
    double t=q/p;
    if(p < 0)
    {
        if( t > *t1) *t1=t;
        if( t > *t2) return 0;
    }
    else if(p > 0)
    {
        if( t < *t2) *t2=t;
        if( t < *t1) return 0;
    }
    else if( q < 0) return 0;
    return 1;
}

void LiangBarskyLineClipAndDraw (double x0, double y0,double x1, double y1)
{
	double dx=x1-x0, dy=y1-y0, te=0, tl=1;
	if(cliptest(-dx,x0-xmin,&te,&tl) && cliptest(dx,xmax-x0,&te,&tl) &&
	   cliptest(-dy,y0-ymin,&te,&tl) && cliptest(dy,ymax-y0,&te,&tl))
	{
		if( tl < 1 )
		{
			x1 = x0 + tl*dx;
			y1 = y0 + tl*dy;
		}
		if( te > 0 )
		{
            x0 = x0 + te*dx;
			y0 = y0 + te*dy;
		}

		glColor3f(1, 0, 0);
		glBegin(GL_LINE_LOOP);
		glVertex2f(xmin + 150, ymin + 150);
		glVertex2f(xmax + 150, ymin + 150);
		glVertex2f(xmax + 150, ymax + 150);
		glVertex2f(xmin + 150, ymax + 150);
		glEnd();
		glColor3f(0,0,1);
		glBegin(GL_LINES);
		glVertex2d (x0 + 150, y0 + 150);
		glVertex2d (x1 + 150, y1 + 150);
		glEnd();
	}
}


void display()
{
    double x0=60,y0=20,x1=80,y1=120;
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1,0,0);
    glBegin(GL_LINES);
	glVertex2d (x0, y0);
	glVertex2d (x1, y1);
	glEnd();
    glColor3f(0, 0, 1);
    glBegin(GL_LINE_LOOP);
    glVertex2f(xmin, ymin);
    glVertex2f(xmax, ymin);
    glVertex2f(xmax, ymax);
    glVertex2f(xmin, ymax);
    glEnd();
    LiangBarskyLineClipAndDraw(x0,y0,x1,y1);
    glFlush();
}
void myinit()
{
	glClearColor(1,1,1,1);
	glColor3f(1,0,0);
	glPointSize(1);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0,500,0,500);
}
void main(int argc, char** argv)
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
	glutInitWindowSize(500,500);
	glutInitWindowPosition(0,0);
	glutCreateWindow("Liang Barsky Line Clipping Algorithm");
	glutDisplayFunc(display);
	myinit();
	glutMainLoop();
}
