#include <stdlib.h>
#include <GL/glut.h>
GLfloat vertices[][3] = {
    {-1,-1,-1},{1,-1,-1},{1,1,-1}, {-1,1,-1},
    {-1,-1,1},{1,-1,1}, {1,1,1}, {-1,1,1}
};

GLfloat colors[][3] = {
    {0,0,0},{0,0,1},{0,1,0},{0,1,1},
    {1,0,0},{1,0,1},{1,1,0},{1,1,1}
};
void square(int a, int b, int c , int d)
{
 	glBegin(GL_POLYGON);
	glColor3fv(colors[a]);
	glNormal3fv(vertices[a]);
	glVertex3fv(vertices[a]);
	glColor3fv(colors[b]);
	glNormal3fv(vertices[b]);
	glVertex3fv(vertices[b]);
	glColor3fv(colors[c]);
	glNormal3fv(vertices[c]);
	glVertex3fv(vertices[c]);
	glColor3fv(colors[d]);
	glNormal3fv(vertices[d]);
	glVertex3fv(vertices[d]);
	glEnd();
}
void colorcube()
{
	square(0,1,2,3);
	square(4,5,6,7);
	square(0,1,5,4);
	square(1,2,6,5);
	square(2,3,7,6);
	square(3,0,4,7);
}

static GLfloat theta[] = {0,0,0};
static GLint axis = 2;
static GLdouble viewer[]= {0, 0, 5};

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	gluLookAt(viewer[0],viewer[1],viewer[2], 0, 0, 0, 0, 1, 0);
	glRotatef(theta[0], 1, 0, 0);
	glRotatef(theta[1], 0, 1, 0);
	glRotatef(theta[2], 0, 0, 1);
    colorcube();
    glFlush();
    glutSwapBuffers();
}

void mouse(int btn, int state, int x, int y)
{
	if(btn==GLUT_LEFT_BUTTON && state == GLUT_DOWN) axis = 0;
	if(btn==GLUT_MIDDLE_BUTTON && state == GLUT_DOWN) axis = 1;
	if(btn==GLUT_RIGHT_BUTTON && state == GLUT_DOWN) axis = 2;
	theta[axis] += 2;
	if( theta[axis] > 360 ) theta[axis] -= 360;
	display();
}

void keys(unsigned char key, int x, int y)
{
   if(key == 'x') viewer[0]-= 1;
   if(key == 'X') viewer[0]+= 1;
   if(key == 'y') viewer[1]-= 1;
   if(key == 'Y') viewer[1]+= 1;
   if(key == 'z') viewer[2]-= 1;
   if(key == 'Z') viewer[2]+= 1;
   display();
}

void myReshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(w<=h)
        glFrustum(-2, 2, -2*(GLfloat)h/(GLfloat)w, 2*(GLfloat)h/(GLfloat)w, 2, 20);
    else
        glFrustum(-2, 2, -2*(GLfloat)w/(GLfloat)h, 2*(GLfloat)w/(GLfloat)h, 2, 20);
    glMatrixMode(GL_MODELVIEW);
}

void  main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowPosition(50,50);
    glutInitWindowSize(500, 500);
    glutCreateWindow("Colorcube Viewer");
    glutDisplayFunc(display);
    glutReshapeFunc(myReshape);
    glutKeyboardFunc(keys);
    glutMouseFunc(mouse);
    glEnable(GL_DEPTH_TEST);
    glutMainLoop();
}
